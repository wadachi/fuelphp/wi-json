<?php
/**
 * Wadachi FuelPHP JSON Package
 *
 * An assortment of classes and helpers for working with JSON.
 *
 * @package    wi-json
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

\Autoloader::add_classes([
  'Wi\\Json_Array' => __DIR__.'/classes/json/array.php',
  'Wi\\Json_DateTime' => __DIR__.'/classes/json/datetime.php',
  'Wi\\Json_String' => __DIR__.'/classes/json/string.php',
]);

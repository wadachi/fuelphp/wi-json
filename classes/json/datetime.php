<?php
/**
 * Wadachi FuelPHP JSON Package
 *
 * An assortment of classes and helpers for working with JSON.
 *
 * @package    wi-json
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * JSON日時
 */
final class Json_DateTime extends \DateTime
{
  /**
   * ISO8601文字列式に日時を変換する
   *
   * @access public
   * @return string ISO8601文字列
   */
  public function __toString()// <editor-fold defaultstate="collapsed" desc="...">
  {
    return $this->format(\DateTime::ISO8601);
  }// </editor-fold>
}

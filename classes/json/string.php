<?php
/**
 * Wadachi FuelPHP JSON Package
 *
 * An assortment of classes and helpers for working with JSON.
 *
 * @package    wi-json
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * 文字列の道具
 */
final class Json_String
{
	/**
	 * コンストラクタ
	 *
	 * @access private
	 */
	private function __construct()// <editor-fold defaultstate="collapsed" desc="...">
	{
		// 何もしない
	}// </editor-fold>

	/**
	 * 文字列をキャメルケースに変換する
	 *
	 * @param string $str 文字列
	 * @return string 変換されたキャメルケース文字列
	 */
	public static function to_camelcase($str)// <editor-fold defaultstate="collapsed" desc="...">
	{
		$ucase = function(&$v) { $v = ucwords($v); };

		$parts = explode('_', $str);
		array_walk($parts, $ucase);
		$newStr = implode('', $parts);

		$parts = explode('-', $newStr);
		array_walk($parts, $ucase);
		$newStr = implode('', $parts);

		$newStr{0} = strtolower($newStr{0});

		return $newStr;
	}// </editor-fold>

	/**
	 * 文字列をケバブケースに変換する
	 *
	 * @param string $str 文字列
	 * @return string 変換されたケバブケース文字列
	 */
	public static function to_kebabcase($str)// <editor-fold defaultstate="collapsed" desc="...">
	{
		$newStr = preg_replace('/[A-Z]/', '-$0', $str);
		$newStr = preg_replace('/_/', '-', $newStr);
		$newStr = strtolower($newStr);
		$newStr = ltrim($newStr, '-');

		return $newStr;
	}// </editor-fold>

	/**
	 * 文字列をアンダーラインに変換する
	 *
	 * @param string $str 文字列
	 * @return string 変換されたアンダーライン文字列
	 */
	public static function to_underline($str)// <editor-fold defaultstate="collapsed" desc="...">
	{
		$newStr = preg_replace('/[A-Z]/', '_$0', $str);
		$newStr = preg_replace('/-/', '_', $newStr);
		$newStr = strtolower($newStr);
		$newStr = ltrim($newStr, '_');

		return $newStr;
	}// </editor-fold>
}

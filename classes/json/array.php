<?php
/**
 * Wadachi FuelPHP JSON Package
 *
 * An assortment of classes and helpers for working with JSON.
 *
 * @package    wi-json
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * JSON配列の道具
 */
final class Json_Array
{
	/**
	 * コンストラクタ
	 *
	 * @access private
	 */
	private function __construct()// <editor-fold defaultstate="collapsed" desc="...">
	{
		// 何もしない
	}// </editor-fold>

	/**
	 * プロパティ名をキャメルケースに変換する
	 *
	 * @param array $array 配列
	 * @return array 変換されたキャメルケース配列
	 */
	public static function to_camelcase(array $array)// <editor-fold defaultstate="collapsed" desc="...">
	{
		$ucase = function(&$v) { $v = ucwords($v); };
		$camelcase_array = [];

		foreach ($array as $key => $value)
		{
			$newKey = explode('_', $key);
			array_walk($newKey, $ucase);
			$newKey = implode('', $newKey);

			$newKey = explode('-', $newKey);
			array_walk($newKey, $ucase);
			$newKey = implode('', $newKey);

			$newKey{0} = strtolower($newKey{0});

			if ( ! is_array($value))
			{
				$camelcase_array[$newKey] = $value;
			}
			else
			{
				$camelcase_array[$newKey] = static::to_camelcase($value);
			}

		}

		return $camelcase_array;
	}// </editor-fold>

	/**
	 * プロパティ名をケバブケースに変換する
	 *
	 * @param array $array 配列
	 * @return array 変換されたケバブケース配列
	 */
	public static function to_kebabcase(array $array)// <editor-fold defaultstate="collapsed" desc="...">
	{
		$kebab_array = [];

		foreach ($array as $key => $value)
		{
			$newKey = preg_replace('/[A-Z]/', '-$0', $key);
			$newKey = preg_replace('/_/', '-', $newKey);
			$newKey = strtolower($newKey);
			$newKey = ltrim($newKey, '-');

			if ( ! is_array($value))
			{
				$kebab_array[$newKey] = $value;
			}
			else
			{
				$kebab_array[$newKey] = static::to_kebabcase($value);
			}
		}

		return $kebab_array;
	}// </editor-fold>

	/**
	 * プロパティ名をアンダーラインに変換する
	 *
	 * @param array $array 配列
	 * @return array 変換されたアンダーライン配列
	 */
	public static function to_underline(array $array)// <editor-fold defaultstate="collapsed" desc="...">
	{
		$underscore_array = [];

		foreach ($array as $key => $value)
		{
			$newKey = preg_replace('/[A-Z]/', '_$0', $key);
			$newKey = preg_replace('/-/', '_', $newKey);
			$newKey = strtolower($newKey);
			$newKey = ltrim($newKey, '_');

			if ( ! is_array($value))
			{
				$underscore_array[$newKey] = $value;
			}
			else
			{
				$underscore_array[$newKey] = static::to_underline($value);
			}
		}

		return $underscore_array;
	}// </editor-fold>
}
